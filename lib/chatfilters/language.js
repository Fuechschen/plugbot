var Promise = require('bluebird');

var detector = require('cld');
var config = require('../load_config');
var langfile = require('../../langfile');

module.exports = {
    name: 'Language',
    type: 'language',
    enabled: true,
    strings: {
        warn: langfile.chatfilter.language.warn
    },
    check: function (data) {
        return new Promise(function (resolve, reject) {
            if (config.chatfilter.language.enabled) {
                detector.detect(data.messae, function (err, msg) {
                    if (err) {
                        resolve();
                    } else {
                        if (msg.reliable) {
                            if (config.chatfilter.language.allowed.indexOf(msg.languages[0].code) !== -1) resolve();
                            else reject({type: 'language', points: 0});
                        } else resolve();
                    }
                });
            } else resolve();
        });
    }
};